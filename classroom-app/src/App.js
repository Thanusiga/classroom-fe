import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';

import StudentList from './components/studentList';
import ClassroomsList from './components/classes/classList';
import SubjectsList from './components/subjects/subjectsList';
import StudentEdit from './components/studentEdit'
import StudentDetail from './components/studentDetail'

function App() {
  return (
    <Router>
      <nav className='navbar navbar-expand navbar-dark bg-dark'>
        {/* <a href='/classrooms' className='navbar-brand'>
          CLASS ROOM
        </a> */}
        <div className='navbar-nav mr-auto'>
          <li className='nav-item'>
            <Link to={"/students"} className="nav-link">
              Students
            </Link>
          </li>
          <li className='nav-item'>
            <Link to={"/classrooms"} className="nav-link">
              Classrooms
            </Link>
          </li>
          <li className='nav-item'>
            <Link to={"/subjects"} className="nav-link">
              Subjects
            </Link>
          </li>
          <li className='nav-item'>
            <Link to={"/teachers"} className="nav-link">
              Teachers
            </Link>
          </li>
        </div>
      </nav>

      <div className='container mt-3'>
        <Routes>
          <Route path="/students"  element={<StudentList/>}/>
                    <Route path="/student/:id" element={<StudentEdit/>}/>
                    <Route path="/student/details" element={<StudentDetail/>}/>
          <Route path="/classrooms"  element={<ClassroomsList/>}/>
          <Route path="/subjects"  element={<SubjectsList/>}/>

        </Routes>
      </div>
    </Router>
  );
}

export default App;
