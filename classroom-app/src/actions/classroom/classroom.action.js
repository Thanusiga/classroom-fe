import {
  CREATE_CLASSROOM,
  RETRIEVE_CLASSROOMS,
  UPDATE_CLASSROOM,
  DELETE_CLASSROOM,
  GET_CLASSROOM_BY_ID,
} from "./types";

import ClassroomDataService from "../../services/classroomService";

//name
export const createClassroom = (data) => async (dispatch) => {
  try {
    const res = await ClassroomDataService.create(data);
    dispatch({
      type: CREATE_CLASSROOM,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const retrieveClassroomById = () => async (dispatch) => {
  try {
    const res = await ClassroomDataService.get();

    dispatch({
      type: GET_CLASSROOM_BY_ID,
      payload: res.data,
    });
  } catch (err) {
    console.log(err);
  }
};

export const retrieveClassrooms = () => async (dispatch) => {
    console.log("dispatch hit")
  try {
    
    const res = await ClassroomDataService.getAll();

    dispatch({
      type: RETRIEVE_CLASSROOMS,
      payload: res.data,
    });
  } catch (err) {
    console.log(err);
  }
};

export const updateClassroom = (id, name) => async (dispatch) => {
  try {
    const res = await ClassroomDataService.update(id, name);

    dispatch({
      type: UPDATE_CLASSROOM,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const deleteClassroom = (id) => async (dispatch) => {
  try {
    await ClassroomDataService.remove(id);

    dispatch({
      type: DELETE_CLASSROOM,
      payload: { id },
    });
  } catch (err) {
    console.log(err);
  }
};

