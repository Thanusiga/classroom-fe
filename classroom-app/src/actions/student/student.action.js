import {
  CREATE_STUDENT,
  RETRIEVE_STUDENTS,
  UPDATE_STUDENT,
  DELETE_STUDENT,
  GET_STUDENT_BY_ID,
} from "./types";

import StudentDataService from "../../services/studentService";

//firstName, lastName,email, dOB,contactPerson, contactNumber,classroomId
export const createStudent = (data) => async (dispatch) => {
  try {
    const res = await StudentDataService.create(data);
    dispatch({
      type: CREATE_STUDENT,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const retrieveStudentById = () => async (dispatch) => {
  try {
    const res = await StudentDataService.get();

    dispatch({
      type: GET_STUDENT_BY_ID,
      payload: res.data,
    });
  } catch (err) {
    console.log(err);
  }
};

export const retrieveStudents = () => async (dispatch) => {
    console.log("dispatch hit")
  try {
    
    const res = await StudentDataService.getAll();

    dispatch({
      type: RETRIEVE_STUDENTS,
      payload: res.data,
    });
  } catch (err) {
    console.log(err);
  }
};

export const updateStudent = (id, firstName, lastName,email, dOB,contactPerson, contactNumber,classroomId) => async (dispatch) => {
  try {
    const res = await StudentDataService.update(id, firstName, lastName,email, dOB,contactPerson, contactNumber,classroomId);

    dispatch({
      type: UPDATE_STUDENT,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const deleteStudent = (id) => async (dispatch) => {
  try {
    await StudentDataService.remove(id);

    dispatch({
      type: DELETE_STUDENT,
      payload: { id },
    });
  } catch (err) {
    console.log(err);
  }
};

