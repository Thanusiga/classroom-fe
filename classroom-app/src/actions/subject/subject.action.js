import {
  CREATE_SUBJECT,
  RETRIEVE_SUBJECTS,
  UPDATE_SUBJECT,
  DELETE_SUBJECT,
  GET_SUBJECT_BY_ID,
  GET_ALL_SUB_WITH_TEACHER
} from "./types";

import SubjectDataService from "../../services/subjectService";

//name
export const createSubject = (data) => async (dispatch) => {
  try {
    const res = await SubjectDataService.create(data);
    dispatch({
      type: CREATE_SUBJECT,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const retrieveSubjectById = () => async (dispatch) => {
  try {
    const res = await SubjectDataService.get();

    dispatch({
      type: GET_SUBJECT_BY_ID,
      payload: res.data,
    });
  } catch (err) {
    console.log(err);
  }
};

export const retrieveSubjects = () => async (dispatch) => {
    console.log("dispatch hit")
  try {
    
    const res = await SubjectDataService.getAll();

    dispatch({
      type: RETRIEVE_SUBJECTS,
      payload: res.data,
    });
  } catch (err) {
    console.log(err);
  }
};

export const getAllSubWithTeacher = () => async (dispatch) => {
    console.log("dispatch hit")
  try {
    
    const res = await SubjectDataService.getAllSubWithTeacher();

    dispatch({
      type: GET_ALL_SUB_WITH_TEACHER,
      payload: res.data,
    });
  } catch (err) {
    console.log(err);
  }
};

export const updateSubject = (id, name) => async (dispatch) => {
  try {
    const res = await SubjectDataService.update(id, name);

    dispatch({
      type: UPDATE_SUBJECT,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const deleteSubject = (id) => async (dispatch) => {
  try {
    await SubjectDataService.remove(id);

    dispatch({
      type: DELETE_SUBJECT,
      payload: { id },
    });
  } catch (err) {
    console.log(err);
  }
};

