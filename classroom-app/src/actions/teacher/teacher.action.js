import {
  CREATE_TEACHER,
  RETRIEVE_TEACHERS,
  UPDATE_TEACHER,
  DELETE_TEACHER,
  GET_TEACHER_BY_ID,
} from "./types";

import TeacherDataService from "../../services/teacherService";

//firstName, lastName,email, dOB,contactPerson, contactNumber,classroomId
export const createTeacher = (data) => async (dispatch) => {
  try {
    const res = await TeacherDataService.create(data);
    dispatch({
      type: CREATE_TEACHER,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const retrieveTeacherById = () => async (dispatch) => {
  try {
    const res = await TeacherDataService.get();

    dispatch({
      type: GET_TEACHER_BY_ID,
      payload: res.data,
    });
  } catch (err) {
    console.log(err);
  }
};

export const retrieveTeachers = () => async (dispatch) => {
    console.log("dispatch hit")
  try {
    
    const res = await TeacherDataService.getAll();

    dispatch({
      type: RETRIEVE_TEACHERS,
      payload: res.data,
    });
  } catch (err) {
    console.log(err);
  }
};

export const updateTeacher = (id, firstName, lastName,email, dOB,contactPerson, contactNumber,classroomId) => async (dispatch) => {
  try {
    const res = await TeacherDataService.update(id, firstName, lastName,email, dOB,contactPerson, contactNumber,classroomId);

    dispatch({
      type: UPDATE_TEACHER,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const deleteTeacher = (id) => async (dispatch) => {
  try {
    await TeacherDataService.remove(id);

    dispatch({
      type: DELETE_TEACHER,
      payload: { id },
    });
  } catch (err) {
    console.log(err);
  }
};

