import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { createClassroom, deleteClassroom, retrieveClassrooms, updateClassroom } from "../../actions/classroom/classroom.action";
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from "yup";
import { useForm } from 'react-hook-form';
import { Link } from "react-router-dom";

import {
    Table, Button, Modal, ModalHeader, ModalBody, ModalFooter, Dropdown, DropdownMenu, DropdownToggle,
    Label, input,
    DropdownItem
} from "reactstrap";


const ClassroomsList = () => {

    const classrooms = useSelector(state => state.classrooms);

    const [isOpen, setIsOpen] = useState(false);
    const [currentClassroom, setCurrentClassroom] = useState(null);
    const [currentIndex, setCurrentIndex] = useState(-1);
    const [isEditMode, setIseditMode] = useState(false);

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(retrieveClassrooms());
    }, [dispatch]);


    const toggle = () => {
        setCurrentClassroom(null);
        dispatch(retrieveClassrooms());

        setIsOpen(!isOpen);
        setIseditMode(false);

    };


    const toggleEdit = () => {
        setCurrentClassroom(null);
        dispatch(retrieveClassrooms());

        setIseditMode(!isEditMode);
    };


    const onError = (errors) => {
        console.error(errors)
    }

    const refreshData = () => {
        setCurrentClassroom(null);
        setCurrentIndex(-1);
    };



    const validationSchema = Yup.object().shape({
        name: Yup.string().required("name is required")
    });


    const {
        register,
        handleSubmit,
        formState: { errors },
        reset,
        setValue
    } = useForm({
        resolver: yupResolver(validationSchema)
    })


    const removeClassroom = (id) => {

        // eslint-disable-next-line no-restricted-globals
        if (!confirm("Want to delete?")) {
            return;
        }
        dispatch(deleteClassroom(id))
            .then(response => {
                console.log(response);
                refreshData();
            })
            .catch(e => {
                console.log(e);
            });

    };


    const editClassroom = (classroom, index) => {
        setActiveClassroom(classroom, index)
        setIseditMode(true);
        console.log(classroom);
    };


    const setActiveClassroom = (classroom, index) => {
        setCurrentClassroom(classroom);
        setCurrentIndex(index);
    };


    const onSubmit = async data => {
        console.log(data, "submit data");

        if (isEditMode) {

            dispatch(updateClassroom(currentClassroom.id, data))
                .then(response => {
                    console.log(response);
                    toggle();
                    refreshData();
                    reset(
                        {
                            "id": 0,
                            "name": "",
                        }
                    );
                })
                .catch(e => {
                    console.log(e);
                });
        }
        else
            dispatch(createClassroom(data))
                .then(response => {
                    console.log(response);
                    toggle();
                    refreshData();
                    reset(
                        {
                            "id": 0,
                            "name": "",
                        }
                    );
                })
                .catch(e => {
                    console.log(e);
                });
    };
// onTodoChange(value){
//         this.setState({
//              name: value
//         });
//     }

    return (

        <div className="container">


            <div className="d-flex justify-content-between mb-2">
                <h4>Classroom List
                </h4>
                <Button onClick={() => toggle()}
                    size="md" variant="primary">
                    Register Classroom
                </Button>
            </div>

            <div className="table-wrapper">
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {classrooms && classrooms.map((classroom, index) => (
                            <tr className={"tr" + (index === currentIndex ? "active" : "")}
                                onClick={() => setActiveClassroom(classroom, index)}
                                key={index}>
                                <td>{classroom.name}</td>
                                <td>
                                    <Button className="edit-link" size="sm"
                                        style={{ marginRight: 2 + 'em' }}
                                        onClick={() => editClassroom(classroom, index)}
                                    >
                                        Edit
                                    </Button>
                                    <Button onClick={() => removeClassroom(classroom.id)}
                                        size="sm" variant="danger">
                                        Delete
                                    </Button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </Table>
            </div>

            <Modal isOpen={isOpen} toggle={toggle} >
                <ModalHeader toggle={toggle}> Register Classroom</ModalHeader>
                <ModalBody>

                    <form onSubmit={handleSubmit(onSubmit, onError)}>

                        <div>
                            <Label>Name</Label>
                            <input type="text" {...register('name')} />
                            {errors.name && <p>{errors.name.message}</p>}
                        </div>
                        <Button type="submit"> Register</Button>
                    </form>

                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={toggle}>
                        Cancel
                    </Button>
                </ModalFooter>
            </Modal>

            <Modal isOpen={isEditMode} toggle={toggleEdit} >
                <ModalHeader Edit={toggleEdit}> Register Classroom</ModalHeader>
                <ModalBody>

                    <form onSubmit={handleSubmit(onSubmit, onError)}>

                        <div>
                            <Label>Name</Label>
                            <input type="text"
                             id={'name' + currentIndex}
                             name='name'
                                //  onChange={e => this.onTodoChange(e.target.value)}
                               value={currentClassroom?.name} {...register('name')} />
                            {errors.name && <p>{errors.name.message}</p>}
                        </div>
                        <Button type="submit"> Edit</Button>
                    </form>

                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={toggleEdit}>
                        Cancel
                    </Button>
                </ModalFooter>
            </Modal>
        </div>
    )


}

export default ClassroomsList;