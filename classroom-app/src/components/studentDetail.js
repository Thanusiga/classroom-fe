import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { updateStudent, deleteStudent } from "../actions/student/student.action";
import StudentDataService from "../services/studentService";
// import SubjectDataService from "../services/subjectService";
import ClassroomDataService from "../services/classroomService";
import { useParams } from "react-router-dom";
import {
    Card, CardBody,
    CardTitle
} from 'reactstrap';
import {
    Table, Button, Modal, ModalHeader, ModalBody, ModalFooter, Dropdown, DropdownMenu, DropdownToggle,
    DropdownItem
} from "reactstrap";
import Select from 'react-select';
import { Link } from "react-router-dom";
import { retrieveStudents } from "../actions/student/student.action";


const Student = (props) => {

        const students = useSelector(state => state.students);


    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(retrieveStudents());
    }, [dispatch]);


    const [classroomsOption, setClassroomsOption] = useState([{ label: '', value: 0 }]);
    const [studentsOption, setStudentsOption] = useState([{ label: '', value: 0 }]);
    const [currentclassroomsOption, setCurrentClassroomsOption] = useState([]);
    const [currentStudentOption, setCurrentStudentOption] = useState([]);
    const [teacherAndSubjects, setTeacherAndSubjects] = useState([
        { teacherId: 0, teahcerName: '', subjectId: 0, subjectName: '' }]);

    console.log(props, "prop")
    const initialStudentState = {
        id: 0,
        classroomId: 0,
        email: "",
        contactPerson: "",
        contactNumber: "",
        dob: "",
        lastName: "",
        firstName: "",
        // subjectIds: []
        // teacherSubjectsIds : [],
        StudentSubjectTeachers: []
    };

    const [currentStudent, setCurrentStudent] = useState(initialStudentState);
    const [message, setMessage] = useState("");


    const getStudent = id => {
        StudentDataService.get(id)
            .then(response => {
                setCurrentStudent(response.data);
        setCurrentStudentOption([{ label: response.data.firstName + " " + response.data.lastName, value: id }]);

                setTeacherAndSubjects(response.data.studentSubjectTeachers.map((stsubTea) => (
                    {
                        teacherId: stsubTea.teacherSubjects.teacher.id, teahcerName: stsubTea.teacherSubjects.teacher.firstName,
                        subjectId: stsubTea.teacherSubjects.subject.id, subjectName: stsubTea.teacherSubjects.subject.name
                    }
                )));

                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    };

    useEffect(() => {
        setStudentsOption(students.map((std) => ({ label: std.firstName + " " + std.lastName, value: std.id })));
        getStudent(students[0]?.id);
    }, [students]);
    
    const getClassrooms = () => {
        ClassroomDataService.getAll()
            .then(response => {
                setClassroomsOption(response.data.map((sub) => ({ label: sub.name, value: sub.id })));
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    };


    useEffect(() => {
        getClassrooms();
        console.log("currentStudent for id", currentStudent)
        console.log("teacherAndSubjects for id", teacherAndSubjects)
    }, [currentStudent.id !== 0]);

    useEffect(() => {
        setCurrentClassroomsOption(classroomsOption.filter((cls) => {
            if (cls.value !== 0 && cls.value === currentStudent.classroomId) {
                console.log("cls", cls)
               return { label: cls.label, value: cls.value };
            }
        }))

        console.log(currentclassroomsOption, "currentclassroomsOption")
    }, [classroomsOption]);

    const handleInputChange = event => {
        console.log("eve", event)
        const { name, value } = event.target;
        console.log(name, "name, ", "value", value)
        setCurrentStudent({ ...currentStudent, [name]: value });
    };

    const handleClassChange = (value) => {
        console.log("value", value)
        setCurrentStudent({ ...currentStudent, 'classroomId': value });
    };

    const handleStudentChange = (value) => {
        console.log("value", value);
        getStudent(value);
    };
    // const handleSubjectsChange = (value) => {
    //     console.log("value", value)
    //     setCurrentStudent({ ...currentStudent, 'subjectIds': value });
    // };

    const getCurrentClass = () => {
        classroomsOption.filter((cls) => {
            if (cls.value === currentStudent.classroomId) {
                console.log("cls", cls)
                return cls;
            }
        })
    }

    return (
        <div>
            {currentStudent && (
                <div className="edit-form">
                    <h4>Student</h4>
                    <Card
                        style={{
                            width: '50%'
                        }}
                    >

                        <CardBody>
                            <CardTitle tag="h5">
                                Student Details
                            </CardTitle>

                            <form>
                                <div className="form-group d-flex mb-2">
                                    
                                    <label className="w-50" htmlFor=""> Student</label> 
                                        {
                                        currentStudentOption.length !== 0 &&
                                         <Select
                                            // defaultValue={{label: '8nnA', value: 3}}
                                            defaultValue={currentStudentOption[0]}
                                            options={studentsOption}
                                             id="firstName"
                                        name="firstName"
                                            placeholder={'Select a student'}
                                            isMulti={false}
                                            onChange={(value) => {
                                                console.log(value)
                                                handleStudentChange(value.value)
                                            }}
                                            isClearable
                                            isSearchable
                                            backspaceRemovesValue={true}
                                        />
            }

                                </div>
                                <div className="form-group d-flex mb-2">
                                    <label className="w-50" htmlFor="">Email</label>
                                    <input
                                        type="text"
                                        className="form-control w-50"
                                        id="email"
                                        name="email"
                                        value={currentStudent.email}
                                        onChange={handleInputChange}
                                    />
                                </div>
                                <div className="form-group d-flex mb-2">
                                    <label className="w-50" htmlFor="">Date of birth</label>
                                    <input
                                        type="text"
                                        className="form-control w-50"
                                        id="dob"
                                        name="dob"
                                        value={currentStudent.dob}
                                        onChange={handleInputChange}
                                    />
                                </div>
                                <div className="form-group d-flex mb-2">
                                    <label className="w-50" htmlFor="">Age</label>
                                    <input
                                        type="text"
                                        className="form-control w-50"
                                        id="age"
                                        name="age"
                                        value={currentStudent.age}
                                        onChange={handleInputChange}
                                    />
                                </div>
                                <div className="form-group d-flex mb-2">
                                    <label className="w-50" htmlFor="">Contact number</label>
                                    <input
                                        type="text"
                                        className="form-control w-50"
                                        id="contactNumber"
                                        name="contactNumber"
                                        value={currentStudent.contactNumber}
                                        onChange={handleInputChange}
                                    />
                                </div>
                                <div className="form-group d-flex mb-2">
                                    <label className="w-50" htmlFor="">Contact person</label>
                                    <input
                                        type="text"
                                        className="form-control w-50"
                                        id="contactPerson"
                                        name="contactPerson"
                                        value={currentStudent.contactPerson}
                                        onChange={handleInputChange}
                                    />
                                </div>
                                <div className="form-group d-flex mb-2">
                                    <label className="w-50" htmlFor="">Classroom</label>
                                    {
                                        currentclassroomsOption.length !== 0 &&
                                        <Select
                                            defaultValue={currentclassroomsOption[0]}
                                            options={classroomsOption}
                                            id="classroomId"
                                            name="classroomId"
                                            placeholder={'Select a classroom'}
                                            isMulti={false}
                                            onChange={(value) => {
                                                console.log(value)
                                                handleClassChange(value.value)
                                            }}
                                            isClearable
                                            isSearchable
                                            backspaceRemovesValue={true}
                                        />
                                    }

                                </div>
                            </form>
                        </CardBody>
                    </Card>


                    <div>
                        <h5>Allocated Subjects and Teachers</h5>
                        <Card>
                            <CardTitle>

                            </CardTitle>
                            <CardBody>
                                <Table striped bordered hover>
                                    <thead>
                                        <tr>
                                            <th>Subjects</th>
                                            <th>Teachers</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {teacherAndSubjects && teacherAndSubjects.map((teacherAndSubject, index) => (
                                            <tr
                                                key={index}>
                                                <td>{teacherAndSubject.subjectName}</td>
                                                <td>{teacherAndSubject.teahcerName}</td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </Table>
                            </CardBody>
                        </Card>
                    </div>
                </div>
            )}

        </div>
    );
};

export default Student;
