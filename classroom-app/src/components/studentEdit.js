import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { updateStudent, deleteStudent } from "../actions/student/student.action";
import StudentDataService from "../services/studentService";
import SubjectDataService from "../services/subjectService";
import ClassroomDataService from "../services/classroomService";
import { useParams } from "react-router-dom";
import {
    Card, CardBody,
    CardTitle
} from 'reactstrap';
import {
    Label,
    Table, Button, Modal, ModalHeader, ModalBody, ModalFooter, Dropdown, DropdownMenu, DropdownToggle,
    DropdownItem
} from "reactstrap";
import Select from 'react-select';
import { Link } from "react-router-dom";
import { retrieveSubjects } from '../actions/subject/subject.action';


const StudentEdit = (props) => {

    const { id } = useParams();
    const subjects = useSelector(state => state.subjects);


    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(retrieveSubjects());
    }, [dispatch]);


    const [subjectsOption, setSubjectsOption] = useState([{ label: '', value: 0 }]);
    const [classroomsOption, setClassroomsOption] = useState([{ label: '', value: 0 }]);
    const [currentsubjectsOption, setCurrentSubjectsOption] = useState([{ label: '', value: 0 }]);
    const [currentclassroomsOption, setCurrentClassroomsOption] = useState([]);
    // const [currentclassroomsOption, setCurrentClassroomsOption] = useState([{ label: '', value: 0 }]);
    const [teacherAndSubjects, setTeacherAndSubjects] = useState([
        { teacherId: 0, teacherName: '', subjectId: 0, subjectName: '', teacherAndSubjectId: 0 }]);
    const [selectedSubjects, setSelectedSubjects] = useState([]);
    const [selectedTeacherSubjectIds, setSelectedTeacherSubjectIds] = useState([]);
    const [teacherAndSubjectList, setTeacherAndSubjectList] = useState([]);

    console.log(props, "prop")
    const initialStudentState = {
        id: 0,
        classroomId: 0,
        email: "",
        contactPerson: "",
        contactNumber: "",
        dob: "",
        lastName: "",
        firstName: "",
        // subjectIds: []
        // teacherSubjectsIds : [],
        StudentSubjectTeachers: []
    };

    const [currentStudent, setCurrentStudent] = useState(initialStudentState);
    const [message, setMessage] = useState("");


    const getStudent = id => {
        StudentDataService.get(id)
            .then(response => {
                setCurrentStudent(response.data);
                getAllSubWithTeacher();

                setTeacherAndSubjects(response.data.studentSubjectTeachers.map((stsubTea) => (
                    {
                        teacherId: stsubTea.teacherSubjects.teacher.id, teacherName: stsubTea.teacherSubjects.teacher.firstName,
                        subjectId: stsubTea.teacherSubjects.subject.id, subjectName: stsubTea.teacherSubjects.subject.name,
                        teacherAndSubjectId: stsubTea.teacherSubjects.id
                    }
                )));

                setSelectedTeacherSubjectIds(response.data.studentSubjectTeachers.map((stsubTea) => (stsubTea.teacherSubjectsId)))
                // setTeacherAndSubjectList(response.data.studentSubjectTeachers.map((stsubTea) => (
                //     {
                //         teacherId: stsubTea.teacherSubjects.teacher.id, teacherName: stsubTea.teacherSubjects.teacher.firstName,
                //         subjectId: stsubTea.teacherSubjects.subject.id, subjectName: stsubTea.teacherSubjects.subject.name,
                //         teacherAndSubjectId: stsubTea.id
                //     }
                // )));
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    };


    const getAllSubWithTeacher = () => {
        SubjectDataService.getAllSubWithTeacher()
            .then(response => {
                setTeacherAndSubjectList(response.data.map((stsubTea) => (
                    {
                        teacherId: stsubTea.teacher.id, teacherName: stsubTea.teacher.firstName,
                        subjectId: stsubTea.subject.id, subjectName: stsubTea.subject.name,
                        teacherAndSubjectId: stsubTea.id
                    }
                )));
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    };

    const getClassrooms = () => {
        ClassroomDataService.getAll()
            .then(response => {
                setClassroomsOption(response.data.map((sub) => ({ label: sub.name, value: sub.id })));
            })
            .catch(e => {
                console.log(e);
            });
    };


    useEffect(() => {
        getStudent(id);
    }, [id]);

    useEffect(() => {
        let subArr = [];
        const selectedteaSub = teacherAndSubjects.map((t) => t.subjectId);
        const teacSub = teacherAndSubjectList.map((t) => t.subjectId);
        subjects.map((sub) => {
            if (teacSub.filter(e => e === sub.id).length > 0) {
                console.log(sub.id);
                subArr.push({ label: sub.name, value: sub.id, isDisabled: false })
            } else {
                if (selectedteaSub.includes(sub.id)) {
                    subArr.push({ label: sub.name, value: sub.id, isDisabled: true })
                }
                else {
                    subArr.push({ label: sub.name, value: sub.id, isDisabled: true })
                }
            }
        });

        //         subArr.filter((m) => selectedteaSub.includes(m.value)){
        // subArr.reduce({ label: sub.name, value: sub.id, isDisabled: true })
        //         }

        setSubjectsOption(
            subArr
        );

    }, [subjects, teacherAndSubjectList]);


    useEffect(() => {
        getClassrooms();
    }, [currentStudent.id !== 0]);

    useEffect(() => {
        // debugger
        setCurrentClassroomsOption(classroomsOption.filter((cls) => {
            if (cls.value !== 0 && cls.value === currentStudent.classroomId) {
                console.log("cls", cls)
                return { label: cls.label, value: cls.value };
            }
        }))

        console.log(currentclassroomsOption, "currentclassroomsOption")
    }, [classroomsOption]);

    const handleInputChange = event => {
        console.log("eve", event)
        const { name, value } = event.target;
        console.log(name, "name, ", "value", value)
        setCurrentStudent({ ...currentStudent, [name]: value });
    };

    const handleClassChange = (value) => {
        console.log("value", value)
        setCurrentStudent({ ...currentStudent, 'classroomId': value });
    };

    // const handleSubjectsChange = (value) => {
    //     console.log("value", value)
    //     setCurrentStudent({ ...currentStudent, 'subjectIds': value });
    // };

    const getCurrentClass = () => {
        classroomsOption.filter((cls) => {
            if (cls.value === currentStudent.classroomId) {
                console.log("cls", cls)
                return cls;
            }
        })
    }
    // const getCurrentSubjects = () => {
    //     subjectsOption.filter((cls) => {
    //         if (cls.value === currentStudent.subjectIds) {
    //             console.log("cls", cls)
    //             return cls;
    //         }
    //     })
    // }


    const updateStatus = () => {
        const data = {
            id: currentStudent.id,
            classroomId: currentStudent.classroomId,
            email: currentStudent.email,
            contactPerson: currentStudent.contactPerson,
            contactNumber: currentStudent.contactNumber,
            dob: currentStudent.dob,
            lastName: currentStudent.lastName,
            firstName: currentStudent.firstName,
            subjects: currentStudent.subjects,
        };

        dispatch(updateStudent(currentStudent.id, data))
            .then(response => {
                console.log(response);

                setCurrentStudent({ ...currentStudent });
                setMessage("The status was updated successfully!");
            })
            .catch(e => {
                console.log(e);
            });
    };

    // const updateContent = () => {
    //     dispatch(updateStudent(currentStudent.id, currentStudent))
    //         .then(response => {
    //             console.log(response);

    //             setMessage("The student was updated successfully!");
    //         })
    //         .catch(e => {
    //             console.log(e);
    //         });
    // };

    // const removeStudent = () => {
    //     dispatch(deleteStudent(currentStudent.id))
    //         .then(() => {
    //             props.history.push("/students");
    //         })
    //         .catch(e => {
    //             console.log(e);
    //         });
    // };

    return (
        <div>
            {currentStudent && (
                <div className="edit-form">
                    <div className="d-flex justify-content-between mb-2">
                        <h4>Student</h4>

                        <Button
                            // onClick={() => toggle()}

                            size="md" variant="primary">
                            Update Student- Id:{id}
                        </Button>
                    </div>
                    <Card
                        style={{
                            width: '50%'
                        }}
                    >

                        <CardBody>
                            <CardTitle tag="h5">
                                Student Details
                            </CardTitle>

                            <form>
                                <div className="form-group d-flex mb-2">
                                    <label className="w-50" htmlFor="">First name</label>
                                    <input
                                        type="text"
                                        className="form-control w-50"
                                        id="firstName"
                                        name="firstName"
                                        value={currentStudent.firstName}
                                        onChange={handleInputChange}
                                    />
                                </div>
                                <div className="form-group d-flex mb-2">
                                    <label className="w-50" htmlFor="">Last name</label>
                                    <input
                                        type="text"
                                        className="form-control w-50"
                                        id="lastName"
                                        name="lastName"
                                        value={currentStudent.lastName}
                                        onChange={handleInputChange}
                                    />
                                </div>
                                <div className="form-group d-flex mb-2">
                                    <label className="w-50" htmlFor="">Date of birth</label>
                                    <input
                                        type="text"
                                        className="form-control w-50"
                                        id="dob"
                                        name="dob"
                                        value={currentStudent.dob}
                                        onChange={handleInputChange}
                                    />
                                </div>
                                <div className="form-group d-flex mb-2">
                                    <label className="w-50" htmlFor="">Age</label>
                                    <input
                                        type="text"
                                        className="form-control w-50"
                                        id="age"
                                        name="age"
                                        value={currentStudent.age}
                                        onChange={handleInputChange}
                                    />
                                </div>
                                <div className="form-group d-flex mb-2">
                                    <label className="w-50" htmlFor="">Contact number</label>
                                    <input
                                        type="text"
                                        className="form-control w-50"
                                        id="contactNumber"
                                        name="contactNumber"
                                        value={currentStudent.contactNumber}
                                        onChange={handleInputChange}
                                    />
                                </div>
                                <div className="form-group d-flex mb-2">
                                    <label className="w-50" htmlFor="">Contact person</label>
                                    <input
                                        type="text"
                                        className="form-control w-50"
                                        id="contactPerson"
                                        name="contactPerson"
                                        value={currentStudent.contactPerson}
                                        onChange={handleInputChange}
                                    />
                                </div>
                                <div className="form-group d-flex mb-2">
                                    <label className="w-50" htmlFor="">Classroom</label>
                                    {
                                        currentclassroomsOption.length !== 0 &&
                                        <Select
                                            // defaultValue={{label: '8nnA', value: 3}}
                                            defaultValue={currentclassroomsOption[0]}
                                            options={classroomsOption}
                                            id="classroomId"
                                            name="classroomId"
                                            placeholder={'Select a classroom'}
                                            isMulti={false}
                                            onChange={(value) => {
                                                console.log(value)
                                                handleClassChange(value.value)
                                            }}
                                            isClearable
                                            isSearchable
                                            backspaceRemovesValue={true}
                                        />
                                    }

                                </div>
                                <div className="mb-2">
                                    <Label>Subjects</Label>

                                    <Select
                                        options={subjectsOption}
                                        id="subjects"
                                        placeholder={'Select a subject'}
                                        isMulti={true}
                                        onChange={(value) => {
                                            console.log(value)
                                            // setValue("subjectIds", `${value.id}`, { shouldTouch: true })}}
                                            // setValue("subjectIds", value.map((val) => val.value), { shouldTouch: true })
                                            setSelectedSubjects(value.map((val) => ({ subjectName: val.label, subjectId: val.value })))
                                        }}
                                        isClearable
                                        isSearchable
                                        // @ts-ignore
                                        //   styles={customSelectStyles}
                                        backspaceRemovesValue={true}
                                    />

                                </div>
                                {selectedSubjects.length > 0 &&
                                    <div className="mb-2">
                                        <p>Selected subject - Please allocate a teacher  </p>
                                        <Table striped bordered hover>
                                            <thead>
                                                <tr>
                                                    <th>Subjects</th>
                                                    <th>Teachers</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {selectedSubjects && selectedSubjects.map((teacherAndSubject, index) => (
                                                    <tr
                                                        key={index}>
                                                        <td>{teacherAndSubject.subjectName}</td>
                                                        <td key={index}>
                                                            <Select
                                                                options={(teacherAndSubjectList.filter(element => selectedSubjects.reduce((opt) =>
                                                                    opt.value !== element.subjectId)).filter((ele) => ele.subjectId === teacherAndSubject.subjectId).map((ts) =>
                                                                        ({ label: ts.teacherName, value: ts.teacherId, teacherAndSubjectId: ts.teacherAndSubjectId })))
                                                                }
                                                                id="teacher"
                                                                placeholder={'Select a subject'}
                                                                isMulti={false}
                                                                onChange={(value) => {
                                                                    console.log(value, "teache");
                                                                    setSelectedTeacherSubjectIds([...selectedTeacherSubjectIds, value.teacherAndSubjectId]);
                                                                    // console.log(value)
                                                                    // // setValue("subjectIds", `${value.id}`, { shouldTouch: true })}}
                                                                    // setValue("subjectIds", value.map((val) => val.value), { shouldTouch: true })
                                                                    // setSelectedSubjects(value.map((val) => ({ subjectName: val.label, subjectId: val.value })))
                                                                }}
                                                                isClearable
                                                                isSearchable
                                                                backspaceRemovesValue={true}
                                                            />
                                                        </td>
                                                    </tr>
                                                ))}
                                            </tbody>
                                        </Table>
                                    </div>
                                }

                            </form>
                        </CardBody>
                    </Card>


                    <div>
                        <h5>Allocated Subjects and Teachers</h5>
                        <Card>
                            <CardTitle>

                            </CardTitle>
                            <CardBody>
                                <Table striped bordered hover>
                                    <thead>
                                        <tr>
                                            <th>Subjects</th>
                                            <th>Teachers</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {teacherAndSubjects && teacherAndSubjects.map((teacherAndSubject, index) => (
                                            <tr
                                                key={index}>
                                                <td>{teacherAndSubject.subjectName}</td>
                                                <td>{teacherAndSubject.teacherName}</td>
                                                <td>


                                                    <Button
                                                        onClick={() => {

                                                            setTeacherAndSubjects((current) =>
                                                                current.filter((cur) => cur.teacherAndSubjectId !== teacherAndSubject.teacherAndSubjectId)
                                                            )
                                                            console.log(teacherAndSubject, "teacherAndSubject");
                                                        }}

                                                        size="sm" variant="danger">
                                                        Remove
                                                    </Button>
                                                    {/* <Button 
                                    // onClick={() =>
                                    // //  removeStudent(student.id)
                                    // }
                                        size="sm" variant="danger">
                                        Delete
                                    </Button> */}
                                                </td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </Table>
                            </CardBody>
                        </Card>
                    </div>
                </div>
            )}

        </div>
    );
};

export default StudentEdit;
