import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { retrieveStudents, createStudent, deleteStudent } from "../actions/student/student.action";
import {
    Table, Button, Modal, ModalHeader, ModalBody, ModalFooter, Dropdown, DropdownMenu, DropdownToggle,
    Label, input,
    DropdownItem
} from "reactstrap";
import { Link } from "react-router-dom";
// import { Tooltip } from 'reactstrap';
import * as Yup from "yup";
// import { Formik, Form, Field, ErrorMessage } from "formik";
// import { object, string, number, mixed } from 'yup';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { retrieveClassrooms } from "../actions/classroom/classroom.action";
import Select from 'react-select';
import { retrieveSubjects, getAllSubWithTeacher } from '../actions/subject/subject.action';
import SubjectDataService from "../services/subjectService";

const StudentList = () => {

    const validationSchema = Yup.object().shape({
        firstName: Yup.string().required("First name is required"),
        lastName: Yup.string(),
        dob: Yup.string().required("Date of birth is required"),
        contactNumber: Yup.string().required("Contact number is required"),
        contactPerson: Yup.string().required("Contact person name is required"),
        email: Yup.string()
            .email("You have enter an invalid email address")
            .required("email is required"),
        classroomId: Yup.number()
            .required("please select a classroom is required"),
        subjectIds: Yup.array()
            .required("please select atleast a subject")
    });

    const [currentStudent, setCurrentStudent] = useState(null);
    const [currentIndex, setCurrentIndex] = useState(-1);
    const [isOpen, setIsOpen] = useState(false);
    // const [isShow, setIsShow] = React.useState(false)
    // const [dropdownOpen, setOpen] = React.useState(false);
    // const [currentClassroom, setCurrentClassroom] = useState({ name: '', id: 0 });
    const [subjectsOption, setSubjectsOption] = useState([{ label: '', value: 0, isDisabled: false }]);
    const [classroomsOption, setClassroomsOption] = useState([{ label: '', value: 0 }]);
    // const [teacherAndSubjectList, setTeacherAndSubjectList] = useState([
    //     { teacherId: 0, teahcerName: '', subjectId: 0, subjectName: '' }]);
    const [teacherAndSubjectList, setTeacherAndSubjectList] = useState([]);
    const [selectedSubjects, setSelectedSubjects] = useState([]);
    const [selectedTeacherSubjectIds, setSelectedTeacherSubjectIds] = useState([]);

    const students = useSelector(state => state.students);
    const classrooms = useSelector(state => state.classrooms);
    const subjects = useSelector(state => state.subjects);

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(retrieveStudents());  // () is important
        dispatch(retrieveSubjects());
    }, [dispatch]);


    const {
        register,
        handleSubmit,
        formState: { errors },
        reset,
        setValue
    } = useForm({
        resolver: yupResolver(validationSchema)
    })



    const onSubmit = async data => {
                console.log(data, "submit data");

                data.teacherSubjectsIds = selectedTeacherSubjectIds;
                console.log(data, "submit data");

        dispatch(createStudent(data))
            .then(response => {
                console.log(response);
                toggle();
                refreshData();
                reset(
                    {
                        "classroomId": 0,
                        "email": "",
                        "contactPerson": "",
                        "contactNumber": "",
                        "dob": "",
                        "lastName": "",
                        "firstName": "",
                        "teacherSubjectsIds" : []
                    }
                );
                // setIsOpen(false);
            })
            .catch(e => {
                console.log(e);
            });
    };


    const onError = (errors) => {
        console.error(errors)
    }

    const refreshData = () => {
        setCurrentStudent(null);
        setSelectedSubjects([]);
        setCurrentIndex(-1);
    };
    const toggle = () => {
        setCurrentStudent(null);
        dispatch(retrieveClassrooms());
        getAllSubWithTeacher();
        setSelectedSubjects([]);

        setIsOpen(!isOpen);

    };


    const getAllSubWithTeacher = () => {
        SubjectDataService.getAllSubWithTeacher()
            .then(response => {
                setTeacherAndSubjectList(response.data.map((stsubTea) => (
                    {
                        teacherId: stsubTea.teacher.id, teacherName: stsubTea.teacher.firstName,
                        subjectId: stsubTea.subject.id, subjectName: stsubTea.subject.name,
                        teacherAndSubjectId : stsubTea.id
                    }
                )));
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    };

    useEffect(() => {
        let subArr = [];
        const teacSub = teacherAndSubjectList.map((t) => t.subjectId);
        subjects.map((sub) => {
            if (teacSub.filter(e => e === sub.id).length > 0) {
                console.log(sub.id);
                subArr.push({ label: sub.name, value: sub.id, isDisabled: false })
            } else {
                subArr.push({ label: sub.name, value: sub.id, isDisabled: true })
            }
        })

        setSubjectsOption(
            subArr
            // teacherAndSubjectList.map((sub) => ({ label: sub.subjectName, value: sub.subjectId }))
        );
        setClassroomsOption(
            classrooms.map((cls) => ({ label: cls.name, value: cls.id }))
        )
    }, [classrooms, subjects, teacherAndSubjectList]);


    const removeStudent = (id) => {

        // eslint-disable-next-line no-restricted-globals
        if (!confirm("Want to delete?")) {
            return;
        }
        dispatch(deleteStudent(id))
            .then(response => {
                console.log(response);
                refreshData();
            })
            .catch(e => {
                console.log(e);
            });

    };

    const setActiveStudent = (student, index) => {
        setCurrentStudent(student);
        setCurrentIndex(index);
    };

    const getTeacherForSubject = () => {
        console.log(teacherAndSubjectList.filter(element => selectedSubjects.reduce((opt) =>
                                                            opt.value !== element.subjectId)).map((ts) =>
                                                                ({ label: ts.teacherName, value: ts.teacherId, subjectId :ts.subjectId  })), "hsdkvfs")
    }

    return (

        <div className="container">


            <div className="d-flex justify-content-between mb-2">
                <h4>Student List
                </h4>

                <Button onClick={() => toggle()}
                    size="md" variant="primary">
                    Register Student
                </Button>
            </div>

            <div className="table-wrapper">
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>First name</th>
                            <th>Last name</th>
                            <th>Email</th>
                            <th>DOB</th>
                            <th>Age</th>
                            <th>Classroom Name</th>
                            <th>Contact person</th>
                            <th>Contact Number</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {students && students.map((student, index) => (
                            <tr className={"tr" + (index === currentIndex ? "active" : "")}
                                onClick={() => setActiveStudent(student, index)}
                                key={index}>
                                <td>{student.firstName}</td>
                                <td>{student.lastName}</td>
                                <td>{student.email}</td>
                                <td>{student.dob}</td>
                                <td>{student.age}</td>
                                <td>{student.classroom?.name}</td>
                                <td>{student.contactPerson}</td>
                                <td>{student.contactNumber}</td>
                                <td>
                                    <Link className="edit-link"
                                        to={"/student/" + student.id}
                                                                               style={{marginRight: 2 + 'em'}}
>
                                        Edit
                                    </Link>
                                    <Button onClick={() => removeStudent(student.id)}
                                        size="sm" variant="danger">
                                        Delete
                                    </Button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </Table>
            </div>

            <Modal isOpen={isOpen} toggle={toggle} >
                <ModalHeader toggle={toggle}> Register Student</ModalHeader>
                <ModalBody>

                    <form onSubmit={handleSubmit(onSubmit, onError)}>

                        <div>
                            <Label>First Name</Label>
                            <input type="text" {...register('firstName')} />
                            {errors.firstName && <p>{errors.firstName.message}</p>}
                        </div>
                        <div style={{ marginBottom: 10 }}>
                            <Label>Last Name</Label>
                            <input type="text" {...register('lastName')} />
                            {errors.lastName && <p>{errors.lastName.message}</p>}
                        </div>
                        <div style={{ marginBottom: 10 }}>
                            <Label>Email</Label>
                            <input type="text" {...register('email')} />
                            {errors.email && <p>{errors.email.message}</p>}
                        </div>

                        <div>
                            <Label>Date of birth</Label>
                            <input type="date" {...register('dob')} />
                            {errors.dob && <p>{errors.dob.message}</p>}
                        </div>
                        <div>
                            <Label>Contact Person</Label>
                            <input type="text" {...register('contactPerson')} />
                            {errors.contactPerson && <p>{errors.contactPerson.message}</p>}
                        </div>
                        <div>
                            <Label>Contact Number</Label>
                            <input type="text" {...register('contactNumber')} />
                            {errors.contactNumber && <p>{errors.contactNumber.message}</p>}
                        </div>
                        <div >
                            <Label>Classroom</Label>
                            {/* <Dropdown toggle={() => { setOpen(!dropdownOpen) }}
                                isOpen={dropdownOpen}>
                                <DropdownToggle className="bg-primary" caret>
                                    {currentClassroom.name != '' ? currentClassroom.name
                                        : 'Select a classroom'}
                                </DropdownToggle>
                                <DropdownMenu>
                                    {classrooms && classrooms.map((cls, index) => (
                                        <DropdownItem dropdownvalue={cls.id}
                                            key={index} {...register('classroomId')} onClick={() => {
                                                setCurrentClassroom({ name: cls.name, id: cls.id })
                                                setValue("classroomId", `${cls.id}`, { shouldTouch: true });
                                            }} >
                                            {cls.name}
                                        </DropdownItem>
                                    ))}
                                </DropdownMenu>
                            </Dropdown> */}


                            <Select
                                // @ts-ignore
                                options={classroomsOption}
                                id="classroom"
                                placeholder={'Select a classroom'}
                                isMulti={false}
                                onChange={(value) => {
                                    console.log(value)
                                    // setValue("subjectIds", `${value.id}`, { shouldTouch: true })}}
                                    setValue("classroomId", value.value, { shouldTouch: true })
                                }}
                                isClearable
                                isSearchable
                                // @ts-ignore
                                //   styles={customSelectStyles}
                                backspaceRemovesValue={true}
                            />
                            {errors.classroomId && <p>{errors.classroomId.message}</p>}

                        </div >
                        <div className="mb-2">
                            <Label>Subjects</Label>

                            <Select
                                options={subjectsOption}
                                id="subjects"
                                placeholder={'Select a subject'}
                                isMulti={true}
                                onChange={(value) => {
                                    console.log(value)
                                    // setValue("subjectIds", `${value.id}`, { shouldTouch: true })}}
                                    setValue("subjectIds", value.map((val) => val.value), { shouldTouch: true })
                                    setSelectedSubjects(value.map((val) => ({ subjectName: val.label, subjectId: val.value })))
                                }}
                                isClearable
                                isSearchable
                                // @ts-ignore
                                //   styles={customSelectStyles}
                                backspaceRemovesValue={true}
                            />
                            {errors.subjectIds && <p>{errors.subjectIds.message}</p>}

                        </div>
                        {selectedSubjects.length > 0 &&
                            <div className="mb-2">
                                <p>Selected subject - Please allocate a teacher  </p>
                                <Table striped bordered hover>
                                    <thead>
                                        <tr>
                                            <th>Subjects</th>
                                            <th>Teachers</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {selectedSubjects && selectedSubjects.map((teacherAndSubject, index) => (
                                            <tr
                                                key={index}>
                                                <td>{teacherAndSubject.subjectName}</td>
                                                <td key={index}>
                                                    <Select
                                                        // isOptionDisabled={() => teacherAndSubjectList.length > 0 && teacherAndSubjectList.every(element => subjectsOption.some((opt) => opt.value !== element.subjectId))}
                                                        options={(teacherAndSubjectList.filter(element => selectedSubjects.reduce((opt) =>
                                                            opt.value !== element.subjectId )).filter((ele) => ele.subjectId === teacherAndSubject.subjectId).map((ts) =>
                                                                ({ label: ts.teacherName, value: ts.teacherId, teacherAndSubjectId :ts.teacherAndSubjectId  })))
                                                            }
                                                        id="teacher"
                                                        placeholder={'Select a subject'}
                                                        isMulti={false}
                                                        onChange={(value) => {
                                                            console.log(value, "teache");
                                                            setSelectedTeacherSubjectIds([...selectedTeacherSubjectIds, value.teacherAndSubjectId]);
                                                            // console.log(value)
                                                            // // setValue("subjectIds", `${value.id}`, { shouldTouch: true })}}
                                                            // setValue("subjectIds", value.map((val) => val.value), { shouldTouch: true })
                                                            // setSelectedSubjects(value.map((val) => ({ subjectName: val.label, subjectId: val.value })))
                                                        }}
                                                        isClearable
                                                        isSearchable
                                                        backspaceRemovesValue={true}
                                                    />
                                                </td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </Table>
                            </div>
                        }

                        <Button type="submit"> Register</Button>
                    </form>

                </ModalBody>
                <ModalFooter>
                    {/* <Button color="primary" onClick={toggle}>
                        Register
                    </Button>{' '} */}
                    <Button color="primary" onClick={toggle}>
                        Cancel
                    </Button>
                </ModalFooter>
            </Modal>
        </div>
    )

}

export default StudentList;

// TOOD :  mulit select subject