import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { createSubject, deleteSubject, retrieveSubjects, updateSubject } from "../../actions/subject/subject.action";
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from "yup";
import { useForm } from 'react-hook-form';
import { Link } from "react-router-dom";

import {
    Table, Button, Modal, ModalHeader, ModalBody, ModalFooter, Dropdown, DropdownMenu, DropdownToggle,
    Label, input,
    DropdownItem
} from "reactstrap";


const SubjectsList = () => {

    const subjects = useSelector(state => state.subjects);

    const [isOpen, setIsOpen] = useState(false);
    const [currentSubject, setCurrentSubject] = useState(null);
    const [currentIndex, setCurrentIndex] = useState(-1);
    const [isEditMode, setIseditMode] = useState(false);

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(retrieveSubjects());
    }, [dispatch]);


    const toggle = () => {
        setCurrentSubject(null);
        dispatch(retrieveSubjects());

        setIsOpen(!isOpen);
        setIseditMode(false);

    };


    const toggleEdit = () => {
        setCurrentSubject(null);
        dispatch(retrieveSubjects());

        setIseditMode(!isEditMode);
    };


    const onError = (errors) => {
        console.error(errors)
    }

    const refreshData = () => {
        setCurrentSubject(null);
        setCurrentIndex(-1);
    };



    const validationSchema = Yup.object().shape({
        name: Yup.string().required("name is required")
    });


    const {
        register,
        handleSubmit,
        formState: { errors },
        reset,
        setValue
    } = useForm({
        resolver: yupResolver(validationSchema)
    })


    const removeSubject = (id) => {

        // eslint-disable-next-line no-restricted-globals
        if (!confirm("Want to delete?")) {
            return;
        }
        dispatch(deleteSubject(id))
            .then(response => {
                console.log(response);
                refreshData();
            })
            .catch(e => {
                console.log(e);
            });

    };


    const editSubject = (subject, index) => {
        setActiveSubject(subject, index)
        setIseditMode(true);
        console.log(subject);
    };


    const setActiveSubject = (subject, index) => {
        setCurrentSubject(subject);
        setCurrentIndex(index);
    };


    const onSubmit = async data => {
        console.log(data, "submit data");

        if (isEditMode) {

            dispatch(updateSubject(currentSubject.id, data))
                .then(response => {
                    console.log(response);
                    toggle();
                    refreshData();
                    reset(
                        {
                            "id": 0,
                            "name": "",
                        }
                    );
                })
                .catch(e => {
                    console.log(e);
                });
        }
        else
            dispatch(createSubject(data))
                .then(response => {
                    console.log(response);
                    toggle();
                    refreshData();
                    reset(
                        {
                            "id": 0,
                            "name": "",
                        }
                    );
                })
                .catch(e => {
                    console.log(e);
                });
    };
// onTodoChange(value){
//         this.setState({
//              name: value
//         });
//     }

    return (

        <div className="container">


            <div className="d-flex justify-content-between mb-2">
                <h4>Subject List
                </h4>
                <Button onClick={() => toggle()}
                    size="md" variant="primary">
                    Register Subject
                </Button>
            </div>

            <div className="table-wrapper">
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {subjects && subjects.map((subject, index) => (
                            <tr className={"tr" + (index === currentIndex ? "active" : "")}
                                onClick={() => setActiveSubject(subject, index)}
                                key={index}>
                                <td>{subject.name}</td>
                                <td>
                                    <Button className="edit-link" size="sm"
                                        style={{ marginRight: 2 + 'em' }}
                                        onClick={() => editSubject(subject, index)}
                                    >
                                        Edit
                                    </Button>
                                    <Button onClick={() => removeSubject(subject.id)}
                                        size="sm" variant="danger">
                                        Delete
                                    </Button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </Table>
            </div>

            <Modal isOpen={isOpen} toggle={toggle} >
                <ModalHeader toggle={toggle}> Register Subject</ModalHeader>
                <ModalBody>

                    <form onSubmit={handleSubmit(onSubmit, onError)}>

                        <div>
                            <Label>Name</Label>
                            <input type="text" {...register('name')} />
                            {errors.name && <p>{errors.name.message}</p>}
                        </div>
                        <Button type="submit"> Register</Button>
                    </form>

                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={toggle}>
                        Cancel
                    </Button>
                </ModalFooter>
            </Modal>

            <Modal isOpen={isEditMode} toggle={toggleEdit} >
                <ModalHeader Edit={toggleEdit}> Register Subject</ModalHeader>
                <ModalBody>

                    <form onSubmit={handleSubmit(onSubmit, onError)}>

                        <div>
                            <Label>Name</Label>
                            <input type="text"
                             id={'name' + currentIndex}
                             name='name'
                                //  onChange={e => this.onTodoChange(e.target.value)}
                               value={currentSubject?.name} {...register('name')} />
                            {errors.name && <p>{errors.name.message}</p>}
                        </div>
                        <Button type="submit"> Edit</Button>
                    </form>

                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={toggleEdit}>
                        Cancel
                    </Button>
                </ModalFooter>
            </Modal>
        </div>
    )


}

export default SubjectsList;