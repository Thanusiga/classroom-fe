import {
  CREATE_CLASSROOM,
  RETRIEVE_CLASSROOMS,
  UPDATE_CLASSROOM,
  DELETE_CLASSROOM,
  GET_CLASSROOM_BY_ID,
} from "../actions/classroom/types";

const initialState = [];

const classroomReducer = (classrooms = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case CREATE_CLASSROOM:
      return [...classrooms, payload];

    case RETRIEVE_CLASSROOMS:
      return payload;

    case UPDATE_CLASSROOM:
      return classrooms.map((classroom) => {
        if (classroom.id === payload.id) {
          return {
            ...classroom,
            ...payload,
          };
        } else {
          return classroom;
        }
      });

    case DELETE_CLASSROOM:
      return classrooms.filter(({ id }) => id !== payload.id);

    case GET_CLASSROOM_BY_ID:
      return payload;

    default:
      return classrooms;
  }
};

export default classroomReducer;