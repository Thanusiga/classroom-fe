import { combineReducers } from "redux";
import students from "./student.reducer";
import classrooms from "./classroom.reducer";
import subjects from "./subject.reducer";
import teachers from "./teacher.reducer";

export default combineReducers({
  students,
  classrooms,
  subjects,
  teachers
});
