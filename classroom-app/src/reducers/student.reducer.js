import {
  CREATE_STUDENT,
  RETRIEVE_STUDENTS,
  UPDATE_STUDENT,
  DELETE_STUDENT,
  GET_STUDENT_BY_ID,
} from "../actions/student/types";

const initialState = [];

const studentReducer = (students = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case CREATE_STUDENT:
      return [...students, payload];

    case RETRIEVE_STUDENTS:
      return payload;

    case UPDATE_STUDENT:
      return students.map((student) => {
        if (student.id === payload.id) {
          return {
            ...student,
            ...payload,
          };
        } else {
          return student;
        }
      });

    case DELETE_STUDENT:
      return students.filter(({ id }) => id !== payload.id);

    case GET_STUDENT_BY_ID:
      return payload;

    default:
      return students;
  }
};

export default studentReducer;