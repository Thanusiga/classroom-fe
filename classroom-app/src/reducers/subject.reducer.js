import {
  CREATE_SUBJECT,
  RETRIEVE_SUBJECTS,
  UPDATE_SUBJECT,
  DELETE_SUBJECT,
  GET_SUBJECT_BY_ID,
  GET_ALL_SUB_WITH_TEACHER
} from "../actions/subject/types";

const initialState = [];

const subjectReducer = (subjects = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case CREATE_SUBJECT:
      return [...subjects, payload];

    case RETRIEVE_SUBJECTS:
      return payload;

    case UPDATE_SUBJECT:
      return subjects.map((subject) => {
        if (subject.id === payload.id) {
          return {
            ...subject,
            ...payload,
          };
        } else {
          return subject;
        }
      });

    case DELETE_SUBJECT:
      return subjects.filter(({ id }) => id !== payload.id);

    case GET_SUBJECT_BY_ID:
      return payload;

    case GET_ALL_SUB_WITH_TEACHER:
      return payload;

    default:
      return subjects;
  }
};

export default subjectReducer;