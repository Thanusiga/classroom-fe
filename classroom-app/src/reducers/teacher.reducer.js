import {
  CREATE_TEACHER,
  RETRIEVE_TEACHERS,
  UPDATE_TEACHER,
  DELETE_TEACHER,
  GET_TEACHER_BY_ID,
} from "../actions/teacher/types";

const initialState = [];

const teacherReducer = (teachers = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case CREATE_TEACHER:
      return [...teachers, payload];

    case RETRIEVE_TEACHERS:
      return payload;

    case UPDATE_TEACHER:
      return teachers.map((teacher) => {
        if (teacher.id === payload.id) {
          return {
            ...teacher,
            ...payload,
          };
        } else {
          return teacher;
        }
      });

    case DELETE_TEACHER:
      return teachers.filter(({ id }) => id !== payload.id);

    case GET_TEACHER_BY_ID:
      return payload;

    default:
      return teachers;
  }
};

export default teacherReducer;