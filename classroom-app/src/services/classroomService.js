import http from "../http-common";


const basePath = 'classroom';

const getAll = () => {
  return http.get(`/${basePath}`);
};

const get = id => {
  return http.get(`/${basePath}/${id}`);
};

const create = data => {
  return http.post(`/${basePath}`, data);
};

const update = (id, data) => {
  return http.put(`/${basePath}/${id}`, data);
};

const remove = id => {
  return http.delete(`/${basePath}/${id}`);
};


const ClassroomService = {
  getAll,
  get,
  create,
  update,
  remove,
};

export default ClassroomService;
