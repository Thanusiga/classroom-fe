import http from "../http-common";


const basePath = 'subject';

const getAll = () => {
  return http.get(`/${basePath}`);
};

const getAllSubWithTeacher = () => {
  return http.get(`/${basePath}/get-sub-with-teachers`);
};

const get = id => {
  return http.get(`/${basePath}/${id}`);
};

const create = data => {
  return http.post(`/${basePath}`, data);
};

const update = (id, data) => {
  return http.put(`/${basePath}/${id}`, data);
};

const remove = id => {
  return http.delete(`/${basePath}/${id}`);
};


const SubjectService = {
  getAll,
  get,
  create,
  update,
  remove,
  getAllSubWithTeacher
};

export default SubjectService;
